var auth;
function init_index(){

	authtoken = false;
	checkauth();

	$("#avatarWrapper").on("click",function(){
		location.href="/";
		checkauth();
	});

	$("#tab-ingr").on('click',function(){
		if(authtoken){
			tabingr("",1);
		}
	});

    $("#tab-reci").on('click',function(){
		if(authtoken){
			tabreci("",1);
		}
	});
	
    $("#tab-pres").on('click',function(){
        if(authtoken){
			tabpres("",1);
		}
	});
	
	$("#tab-clie").on('click',function(){
        if(authtoken){
			tabclie("",1);
		}
	});

	$("#tab-sets").on('click',function(){
        if(authtoken){
			tabsets("",1);
		}
    });
}

function tabingr(search, page){
	$.ajax({
		type: 'POST',
		url: "ingr/",
		data:{
			filter:search,
			page:page
		}
	}).done(function (data){
		$('#content-wrapper').html(data);
		$("#previngr").on("click",function(){
			if(page == 1){
				page++;
			}
			tabingr(search,page-1);
		});
		$("#nextingr").on("click",function(){
			tabingr(search,page+1);
		});
		//FILTER//
		$("#searchbtn").on('click',function(e){
			tabingr($("#filter").val(),1);
		});
		//DELETE//
		$(".delete").on('click',function(e){
			console.log("delete");
			$.ajax({
				type: 'POST',
				url: "ingr/del",
				data: {
					guid : $(this).parent().parent().attr("id"),
				}
			}).done(function (data){
				if(data == "success"){
					tabingr("",1);
				}
			});
		});
		//EDIT//
		$(".edit").on('click',function(e){
			$(this).parent().parent().find(".nameDisplay").addClass("hidden");
			$(this).parent().parent().find(".nameInput").removeClass("hidden");
			$(this).parent().parent().find(".priceDisplay").addClass("hidden");
			$(this).parent().parent().find(".priceInput").removeClass("hidden");
			$(this).parent().parent().find(".ingrQuantityDisplay").addClass("hidden");
			$(this).parent().parent().find(".ingrQuantityInput").removeClass("hidden");
			$(this).parent().parent().find(".validate").removeClass("hidden");
			$(this).parent().parent().find(".cancel").removeClass("hidden");
			var guid = $(this).parent().parent().attr("id");
			$("#namePiece").focus();
			$(this).parent().parent().find(".validate").on("click",function(){
				$.ajax({
					type: 'POST',
					url: "ingr/edit",
					data: {
						guid : guid,
						name : $(this).parent().parent().find(".namePiece").val(),
						price : $(this).parent().parent().find(".pricePiece").val(),
						quantity : $(this).parent().parent().find(".quantityPiece").val(),
						unity : $(this).parent().parent().find(".unityPiece").val(),
					}
				}).done(function (data){
					if(data == "success"){
						tabingr("",1);
					}
				});
			});
		});
		//CANCEL//
		$(".cancel").on('click',function(e){
			$(this).parent().parent().find(".nameDisplay").removeClass("hidden");
			$(this).parent().parent().find(".nameInput").addClass("hidden");
			$(this).parent().parent().find(".priceDisplay").removeClass("hidden");
			$(this).parent().parent().find(".priceInput").addClass("hidden");
			$(this).parent().parent().find(".ingrQuantityDisplay").removeClass("hidden");
			$(this).parent().parent().find(".ingrQuantityInput").addClass("hidden");
			$(this).parent().parent().find(".validate").addClass("hidden");
			$(this).parent().parent().find(".cancel").addClass("hidden");
		});
		//ADD POP UP//
		$("#addbtn").on('click',function(){
			$("#popup-wrapper").removeClass("hidden");
			$("#createbtn-wrapper").removeClass("hidden");
			$("#cancelbtn-wrapper").removeClass("hidden");
			$("#addbtn").addClass("hidden");
			$("#nameNewPiece").focus();
		});
		//CANCEL POP UP//
		$("#cancelbtn-wrapper").on('click',function(){
			$("#popup-wrapper").addClass("hidden");
			$("#createbtn-wrapper").addClass("hidden");
			$("#cancelbtn-wrapper").addClass("hidden");
			$("#editbtn-wrapper").addClass("hidden");
			$("#addbtn").removeClass("hidden");
		});
		//CREATE//
		$("#createbtn").on('click',function(){
			$("#popup-wrapper").addClass("hidden");
			$("#createbtn-wrapper").addClass("hidden");
			$("#cancelbtn-wrapper").addClass("hidden");
			$("#addbtn").removeClass("hidden");
			$.ajax({
				type: 'POST',
				url: "ingr/add",
				data: {
					name : $("#nameNewPiece").val(),
					price : 0,
					quantity : 1,
					unity : "Kg",
				}
			}).done(function (data){
				if(data == "success"){
					tabingr("",1);
				}
			});
		});
	});
}

function tabreci(search,page){
	$.ajax({
		type: 'POST',
		url: "reci/",
		data:{
			filter:search,
			page:page
		}
	}).done(function (data){
		$('#content-wrapper').html(data);
		$("#prevreci").on("click",function(){
			console.log("prev");
			if(page == 1){
				page++;
			}
			tabreci(search,page-1);
		});
		$("#nextreci").on("click",function(){
			console.log("next");
			tabreci(search,page+1);
		});
		//FILTER//
		$("#searchbtn").on('click',function(e){
			tabreci($("#filter").val(),1);
		});
		//DELETE//
		$(".delete").on('click',function(e){
			$.ajax({
				type: 'POST',
				url: "reci/del",
				data: {
					guid : $(this).parent().parent().attr("id"),
				}
			}).done(function (data){
				if(data == "success"){
					tabreci("",1);
				}
			});
		});
		//CREATE//
		$("#createbtn").on('click',function(){
			$("#popup-wrapper").addClass("hidden");
			$("#createbtn-wrapper").addClass("hidden");
			$("#cancelbtn-wrapper").addClass("hidden");
			$("#addbtn").removeClass("hidden");
			$.ajax({
				type: 'POST',
				url: "reci/add",
				data: {
					name : $("#nameRecipe").val()
				}
			}).done(function (data){
				if(data == "success"){
					tabreci("",1);
				}
			});
		});
		//ADD POP UP//
		$("#addbtn").on('click',function(){
			$("#popup-wrapper").removeClass("hidden");
			$("#createbtn-wrapper").removeClass("hidden");
			$("#cancelbtn-wrapper").removeClass("hidden");
			$("#addbtn").addClass("hidden");
			$("#namePiece").focus();
		});
		//CANCEL POP UP//
		$("#cancelbtn-wrapper").on('click',function(){
			$("#popup-wrapper").addClass("hidden");
			$("#createbtn-wrapper").addClass("hidden");
			$("#cancelbtn-wrapper").addClass("hidden");
			$("#editbtn-wrapper").addClass("hidden");
			$("#addbtn").removeClass("hidden");
		});
		//EDIT//
		$(".edit").on('click',function(e){
			var guid = $(this).parent().parent().attr("id");
			tabrecidetail(guid);
		});
	});
}

function tabrecidetail(guid){
	$.ajax({
		type: 'POST',
		url: "reci/detail",
		data: {
			guid : guid,
		}
	}).done(function (data){
		$('#content-wrapper').html(data);
		$.ajax({
			type: 'POST',
			url: "reci/allergenics",
			data: {
				guid : guid,
			}
		}).done(function (data){
			var a = data.split(",");
			for (var i = 0; i < a.length; i++) {
				if(a[i] == "true"){
					$("#allergenic-"+parseInt(i+1)+"-on").removeClass("hidden");
				}else{
					$("#allergenic-"+parseInt(i+1)+"-off").removeClass("hidden");
				}
			}
		});
		//ADD INGR IN RECIPE//
		$("#addRecipeIngr").on("click",function(){
			var guidingr = $('option:selected', $("#ingrselect")).attr("guid");
			$.ajax({
				type: 'POST',
				url: "reci/addingr",
				data: {
					guidingr : guidingr,
					guidrecipe : guid
				}
			}).done(function (data){
				tabrecidetail(guid);
			});
		});
		//DELETE INGR IN RECIPE//
		$(".delete").on('click',function(e){
			$.ajax({
				type: 'POST',
				url: "reci/delingr",
				data: {
					guidingr : $(this).parent().parent().attr("id"),
					guidrecipe : guid
				}
			}).done(function (data){
				if(data == "success"){
					tabrecidetail(guid);
				}
			});
		});
		//EDIT INGR USED QUANTITY
		$(".editIngrUsedQuantity").on('click',function(e){
			$(this).parent().parent().find(".usedQuantityDisplay").addClass("hidden");
			$(this).parent().parent().find(".usedQuantityInput").removeClass("hidden");
			$(this).parent().find(".validate").removeClass("hidden");
			$(this).parent().find(".cancel").removeClass("hidden");
			$(this).parent().find(".validate").on('click',function(e){
				$.ajax({
					type: 'POST',
					url: "reci/editingr",
					data: {
						guidingr : $(this).parent().parent().attr("id"),
						guidrecipe : guid,
						usedN : $(this).parent().parent().find(".quantityInput").val(),
						usedU : $(this).parent().parent().find(".unitSelector").val()
					}
				}).done(function (data){
					if(data == "success"){
						tabrecidetail(guid);
					}
				});
			});	
			$(this).parent().find(".cancel").on('click',function(e){
				$(".usedQuantityDisplay").removeClass("hidden");
				$(".usedQuantityInput").addClass("hidden");
				$(".validate").addClass("hidden");
				$(".cancel").addClass("hidden");
			});	
		});
		//ADD RECI IN RECIPE//
		$("#addRecipeReci").on("click",function(){
			console.log("addRecipeReci");
			var guidreci = $('option:selected', $("#reciselect")).attr("guid");
			$.ajax({
				type: 'POST',
				url: "reci/addreci",
				data: {
					guidreci : guidreci,
					guidrecipe : guid
				}
			}).done(function (data){
				tabrecidetail(guid);
			});
		});
		//DELETE RECI IN RECIPE//
		$(".delete").on('click',function(e){
			$.ajax({
				type: 'POST',
				url: "reci/delreci",
				data: {
					guidreci : $(this).parent().parent().attr("id"),
					guidrecipe : guid
				}
			}).done(function (data){
				if(data == "success"){
					tabrecidetail(guid);
				}
			});
		});
		//EDIT RECI USED QUANTITY
		$(".editReciUsedQuantity").on('click',function(e){
			$(this).parent().parent().find(".usedQuantityDisplay").addClass("hidden");
			$(this).parent().parent().find(".usedQuantityInput").removeClass("hidden");
			$(this).parent().find(".validate").removeClass("hidden");
			$(this).parent().find(".cancel").removeClass("hidden");
			$(this).parent().find(".validate").on('click',function(e){
				$.ajax({
					type: 'POST',
					url: "reci/editreci",
					data: {
						guidreci : $(this).parent().parent().attr("id"),
						guidrecipe : guid,
						usedN : $(this).parent().parent().find(".quantityInput").val(),
						usedU : $(this).parent().parent().find(".unitSelector").val()
					}
				}).done(function (data){
					if(data == "success"){
						tabrecidetail(guid);
					}
				});
			});	
			$(this).parent().find(".cancel").on('click',function(e){
				$(".usedQuantityDisplay").removeClass("hidden");
				$(".usedQuantityInput").addClass("hidden");
				$(".validate").addClass("hidden");
				$(".cancel").addClass("hidden");
			});	
		});
		//EDIT ALLERGENICS USED IN RECI
		$(".allergenic-item").on("click", function(){
			var allergenicItem = $(this).attr("id").split("-");
			if(allergenicItem[2]=="on"){
				var onoff = "off";
			}else{
				var onoff = "on";
			}
			$(this).addClass("hidden");
			$("#allergenic-"+allergenicItem[1]+"-"+onoff).removeClass("hidden");
			var allergenicItems = [];
			var selectedAllergenic = $(".allergenic-selected .allergenic-item");
			for (var i = 0; i < selectedAllergenic.length; i++) {
				if($(selectedAllergenic[i]).hasClass("hidden")){
					allergenicItems[i] = false;
				}else{
					allergenicItems[i] = true;
				}
				var allergenicItemsString = allergenicItems.join();
			}
			$.ajax({
				type: 'POST',
				url: "reci/editallergenics",
				data: {
					guidingr : $(this).parent().parent().attr("id"),
					guidrecipe : guid,
					allergenicItemsString : allergenicItemsString
				}
			}).done(function (data){
				console.log("success");
			});
		});
		//EDIT RECIPE OUTPOUT QUANTITY
		$(".outputQuantityDisplay").on('click',function(e){
			$(".outputQuantityDisplay").addClass("hidden");
			$(".outputQuantityInput").removeClass("hidden");
			$(this).parent().find(".cancel").on('click',function(e){
				$(".outputQuantityDisplay").removeClass("hidden");
				$(".outputQuantityInput").addClass("hidden");
			});	
			$(this).parent().find(".validate").on('click',function(e){
				$.ajax({
					type: 'POST',
					url: "reci/editOutput",
					data: {
						guid : guid,
						outputN : $(this).parent().find("#outputQuantityN").val(),
						outputU : $(this).parent().find("#outputQuantityU").val()
					}
				}).done(function (data){
					if(data == "success"){
						tabrecidetail(guid);
					}
				});
			});	
		});
	});
}

function tabpres(){
	$.ajax({
		type: 'POST',
		url: "pres/"
	}).done(function (data){
		$('#content-wrapper').html(data);
	});
}

function tabclie(){
	$.ajax({
		type: 'POST',
		url: "clie/"
	}).done(function (data){
		$('#content-wrapper').html(data);
	});
}

function tabsets(){
	$.ajax({
		type: 'POST',
		url: "sets/"
	}).done(function (data){
		$('#content-wrapper').html(data);
		$(".form-control").on("change",function(){
			$.ajax({
				type: 'POST',
				url: "sets/edit",
				data: {
					month1 : $("#month1").val(),
					month2 : $("#month2").val(),
					month3 : $("#month3").val(),
					month4 : $("#month4").val(),
					month5 : $("#month5").val(),
					month6 : $("#month6").val(),
					month7 : $("#month7").val(),
					month8 : $("#month8").val(),
					month9 : $("#month9").val(),
					month10 : $("#month10").val(),
					month11 : $("#month11").val(),
					month12 : $("#month12").val(),
					marge : $("#marge").val(),
					rent : $("#monthlyRent").val(),
					elec : $("#monthlyElec").val(),
					water : $("#monthlyWater").val()
				}
			}).done(function (data){
				console.log("success");
			});
		});
	});
}

function checkauth(){
	$.ajax({
		type: 'POST',
		url: "checkauth/",
	}).done(function(data){
		if(data == "true"){
			authtoken = true;
			$("#indexpanel").html("<h2>AUTHENTIFICATION REUSSIE</h2><img class='btnimg' src='res/success.png' alt='authentification success'><a class='btn' id='logout'><img src='res/exit.png' alt='auth'/></a>");
			$("#logout").on("click", function(){
				killauth();
			});
		}else{
			authtoken = false;
			$("#indexpanel").html('<div class="form-group"><label for="pass" class="control-label">Mot de passe</label><input type="password" class="form-control" id="password"></div><a class="btn" id="auth"><img src="res/login.png" alt="auth"/></a>');
			$("#auth").on("click", function(){
				tryauth($("#password").val());
			});
			$('#password').keypress(function (e) {
				if(e.which == 13)
				{
					e.preventDefault();
					tryauth($("#password").val());
				}
			});   
		}
	});
}

function tryauth(password){
	$.ajax({
		type: 'POST',
		url: "auth/",
		data:{
			pass:password
		}
	}).done(function(data){
		checkauth();
	});
}

function killauth(){
	$.ajax({
		type: 'POST',
		url: "killauth/",
	}).done(function(){
		checkauth();
	});
}