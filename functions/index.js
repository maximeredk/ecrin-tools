var functions = require('firebase-functions');
var admin = require('firebase-admin');
var http = require('http');
var url = require('url');
var fs = require('fs');
var express = require('express');
var path = require("path");
var bodyParser = require('body-parser');
var sha256 = require("sha256");
var app = express();

//#region CONFIG

const pageSize = 12;
var ingr,reci,pres;
var recipe,prestation,client;
var ingredients,recipes,prestations,clients;
var auth = true;    

app.use("/css",express.static(__dirname + "./public/css"));
app.use("/js",express.static(__dirname + "./public/js"));
app.use("/res",express.static(__dirname + "./public/res"));

app.use(bodyParser.json());       
app.use(bodyParser.urlencoded({
  extended: true
})); 

var key = "E8966D4DFA2AD0448943576C4B8F1E5DC0CB50019BDCD5B0BB7DB9D62FA25EC4";
var config = {
    apiKey: "AIzaSyBW42TJ98z8OY_M8NIXJjDQN8JXAjpJe_U",
    authDomain: "ecrin-de-gourmandises.firebaseapp.com",
    databaseURL: "https://ecrin-de-gourmandises-dev.firebaseio.com",
    projectId: "ecrin-de-gourmandises",
    storageBucket: "ecrin-de-gourmandises.appspot.com",
    messagingSenderId: "959552237300"
};
admin.initializeApp(config);

app.use(function (req, res, next){
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,data');
    if(req.method==="OPTIONS"){
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,data');
        res.setHeader('Access-Control-Allow-Credentials', true);
        res.end();
    }else{
        next();
    }
});

app.set('view engine', 'ejs');
app.set('views', 'views');

const dbRefObjectINGR = admin.database().ref().child("ingredients");
const dbRefObjectRECI = admin.database().ref().child("recipes");
const dbRefObjectPRES = admin.database().ref().child("prestations");

//ON VALUE UPDATE
dbRefObjectINGR.on("value", function(snap){
    ingr = new Array();
    for(var ing in snap.val()){
        ingr.push(snap.val()[ing]);
    }
});

dbRefObjectRECI.on("value", function(snap){
    reci = new Array();
    for(var rec in snap.val()){
        reci.push(snap.val()[rec]);
    }
});

dbRefObjectPRES.on("value", function(snap){
    pres = new Array();
    for(var pre in snap.val()){
        pres.push(snap.val()[pre]);
    }
});

//#endregion CONFIG

//#region INGREDIENTS
/////////////////
// INGREDIENTS //
/////////////////

//MAIN PAGE INGR
app.post('/ingr/',function(req, res){
    //INGR FILTER
    if(auth){
        var filtered = new Array();
        if(req.body.filter != null && req.body.filter != ""){
            var regexp = new RegExp(req.body.filter,"i");
            for(var ing in ingr){
                if(ingr[ing]["name"].match(regexp)){
                    filtered.push(ingr[ing]);
                }
            }
        }else{
            for(var ing in ingr){
                filtered.push(ingr[ing]);
                filtered[filtered.length-1].price = formatCS(filtered[filtered.length-1].price);
            }
        }
        //INGR SORTING
        filtered.sort(function(a, b){
            if(a["name"] < b["name"]) return -1;
            if(a["name"] > b["name"]) return 1;
            return 0;
        });
        //INGR PAGINATION
        var s = req.body.page*pageSize-pageSize;
        var e = req.body.page*pageSize;
        filtered = filtered.slice(s,e);
        //RENDERING
        res.render('ingr.ejs', {
            data : filtered,
            filter : req.body.filter,
            page : req.body.page
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//ADD INGR ROUTE
app.post('/ingr/add/',function(req, res){
    if(auth){
        var guid = createGuid();
        admin.database().ref('ingredients/' + guid).set({
            guid: guid,
            name: req.body.name,
            price: req.body.price,
            quantity: req.body.quantity,
            unity:req.body.unity
        },function(error){
            if(error){
                console.log("error : " + error);
            }else{
                res.end("success");
            }
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//EDIT INGR ROUTE
app.post('/ingr/edit/',function(req, res){
    if(auth){
        var dbRefObjectRecipesIngr = admin.database().ref('recipesIngredients/');
        var dbRefObjectIngrLookup = admin.database().ref('ingredientsLookup/' + req.body.guid);
        admin.database().ref('ingredients/' + req.body.guid).update({
            guid: req.body.guid,
            name: req.body.name,
            price: req.body.price,
            quantity: req.body.quantity,
            unity:req.body.unity
        }).then(function(){
            dbRefObjectIngrLookup.once("value", snaplookup => {
                snaplookup.forEach(rsnap => {
                    calcRecipeCost(rsnap.key);
                    dbRefObjectRecipesIngr.child(rsnap.key).child(req.body.guid).update({
                        guid: req.body.guid,
                        name: req.body.name,
                        price: req.body.price,
                        quantity: req.body.quantity,
                        unity:req.body.unity
                    }).catch(function(e){
                        console.log("error : " + e.message);
                    });
                });
            });
        }).then(function(){
            res.end("success");
        }).catch(function(e){
            console.log("error : " + e.message);
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//DELETE INGR ROUTE
app.post('/ingr/del/',function(req, res){
    console.log("delINGR");
    if(auth){
        var dbRefObjectRecipesIngr = admin.database().ref('recipesIngredients/');
        var dbRefObjectIngrLookup = admin.database().ref('ingredientsLookup/' + req.body.guid);
        admin.database().ref('ingredients/' + req.body.guid).remove().then(function(){
            dbRefObjectIngrLookup.once("value", snaplookup => {
                snaplookup.forEach(rsnap => {
                    calcRecipeCost(rsnap.key);
                    dbRefObjectRecipesIngr.child(rsnap.key).child(req.body.guid).remove().then(function(){
                        dbRefObjectIngrLookup.remove().catch(function(e){
                            console.log("error : " + e.message);
                        });
                    }).catch(function(e){
                        console.log("error : " + e.message);
                    });
                });
            });
        }).then(function(){
            res.end("success");
        }).catch(function(e){
            console.log("error : " + e.message);
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//#endregion INGREDIENTS

//#region RECIPES

/////////////
// RECIPES //
/////////////

//MAIN PAGE RECIPES
app.post('/reci/',function(req, res){
    //RECI FILTER
    if(auth){
        var filtered = new Array();
        if(req.body.filter != null && req.body.filter != ""){
            var regexp = new RegExp(req.body.filter,"i");
            for(var rec in reci){
                if(reci[rec]["name"].match(regexp)){
                    filtered.push(reci[rec]);
                }
            }
        }else{
            for(var rec in reci){
                filtered.push(reci[rec]);
            }
        }
        //RECI SORTING
        filtered.sort(function(a, b){
            if(a["name"] < b["name"]) return -1;
            if(a["name"] > b["name"]) return 1;
            return 0;
        });
        //RECI PAGINATION
        var s = req.body.page*pageSize-pageSize;
        var e = req.body.page*pageSize;
        filtered = filtered.slice(s,e);
        //RENDERING
        res.render('reci.ejs', {
            data : filtered,
            filter : req.body.filter,
            page : req.body.page
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//ADD RECI ROUTE
app.post('/reci/add/',function(req, res){
    if(auth){
        var guid = createGuid();
        admin.database().ref('recipes/' + guid).set({
            guid: guid,
            name: req.body.name,
            price: 0,
            outputN: 1,
            outputU: "parts"
        },function(error){
            if(error){
                console.log("error : " + error);
            }else{
                res.end("success");
            }
        });
        admin.database().ref('recipes/' + guid +"/allergenics").update({
            1 : "false",
            2 : "false",
            3 : "false",
            4 : "false",
            5 : "false",
            6 : "false",
            7 : "false",
            8 : "false",
            9 : "false",
            10 : "false",
            11 : "false",
            12 : "false",
            13 : "false",
            14 : "false"
        },function(error){
            if(error){
                console.log("error : " + error);
            }else{
                res.end("success");
            }
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//DELETE RECI ROUTE
app.post('/reci/del/',function(req, res){
    if(auth){
        var dbRefObjectRecipesIngr = admin.database().ref('recipesIngredients/' + req.body.guid);
        var dbRefObjectRecipesReci = admin.database().ref('recipesRecipes/' + req.body.guid);
        var dbRefObjectReciLookup = admin.database().ref('recipesLookup/' + req.body.guid);
        admin.database().ref('recipes/' + req.body.guid).remove().then(function(){
            dbRefObjectRecipesIngr.once("value").then(function(snapreciingr){
                snapreciingr.forEach(snapri => {
                    admin.database().ref('ingredientsLookup/').child(snapri.key).child(req.body.guid).remove()
                });
                dbRefObjectRecipesIngr.remove();
            });
        }).then(function(){
            dbRefObjectRecipesReci.once("value").then(function(snaprecireci){
                snaprecireci.forEach(snaprr => {
                    admin.database().ref('recipesLookup/').child(snaprr.key).child(req.body.guid).remove()
                });
                dbRefObjectRecipesReci.remove();
            });
        }).then(function(){
            dbRefObjectReciLookup.once("value").then(function(snaprecilookup){
                snaprecilookup.forEach(snaplkp => {
                    admin.database().ref('recipesRecipes/').child(snaplkp.key).child(req.body.guid).remove()
                    calcRecipeCost(snaplkp.key);
                });
                dbRefObjectReciLookup.remove();
            });
        }).then(function(){
            res.end("success");
        }).catch(function(e){
            console.log("error : " + e.message);
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//DETAIL RECI ROUTE
app.post('/reci/detail/',function(req, res){
    if(auth){
        var sets;
        admin.database().ref().child("settings").on("value", function(snap){
            sets = snap.val();
        });
        var dbRefObjectONERECI = admin.database().ref('recipes/' + req.body.guid);
        dbRefObjectONERECI.once("value").then(function(snapr){
            recipe = snapr.val();
            var dbRefObjectRECIINGR = admin.database().ref('recipesIngredients/' + recipe.guid);
            dbRefObjectRECIINGR.once("value").then(function(snapri){
                ingredients = new Array();
                recipes = new Array();
                var total = 0;
                for(var rIngr in snapri.val()){
                    ingredients.push(snapri.val()[rIngr]);
                    ingredients[ingredients.length-1].cost = formatCS(convertIngrPrice(ingredients[ingredients.length-1].quantity,ingredients[ingredients.length-1].unity,ingredients[ingredients.length-1].price,ingredients[ingredients.length-1].usedN,ingredients[ingredients.length-1].usedU));
                    ingredients[ingredients.length-1].price = formatCS(ingredients[ingredients.length-1].price);
                    total = parseFloat(total) + parseFloat(ingredients[ingredients.length-1].cost);
                }
                var dbRefObjectRECIRECI = admin.database().ref('recipesRecipes/' + recipe.guid);
                dbRefObjectRECIRECI.once("value").then(function(snaprr){
                    for(var rReci in snaprr.val()){
                        recipes.push(snaprr.val()[rReci]);
                        recipes[recipes.length-1].cost = formatCS(convertReciPrice(recipes[recipes.length-1].outputN,recipes[recipes.length-1].outputU,recipes[recipes.length-1].cost,recipes[recipes.length-1].usedN,recipes[recipes.length-1].usedU));
                        recipes[recipes.length-1].price = formatCS(recipes[recipes.length-1].price);
                        total = parseFloat(total) + parseFloat(recipes[recipes.length-1].cost);
                    }
                    //FILTRATION DES SELECT OPTIONS
                    var ingrNotUsed = new Array();
                    var reciNotUsed = new Array();
                    var alreadyUsed;
                    ingr.forEach(ing => {
                        alreadyUsed = false;
                        ingredients.forEach(ingused => {
                            if(ing.guid == ingused.guid){
                                alreadyUsed = true;
                            }
                        });
                        if(!alreadyUsed){
                            ingrNotUsed.push(ing);
                        }
                    });
                    reci.forEach(rec => {
                        alreadyUsed = false;
                        recipes.forEach(recused => {
                            if(rec.guid == recused.guid){
                                alreadyUsed = true;
                            }
                        });
                        if(rec.guid == req.body.guid){
                            alreadyUsed = true;
                        }
                        if(!alreadyUsed){
                            reciNotUsed.push(rec);
                        }
                    });
                    res.render('reciedit.ejs', {
                        data : recipe,
                        ingrused: ingredients,
                        reciused: recipes,
                        ingrs: ingrNotUsed,
                        recis: reciNotUsed,
                        sets: sets,
                        charges : parseFloat(parseFloat((parseFloat(sets["water"])+parseFloat(sets["elec"])+parseFloat(sets["rent"]))/sets["month" + (new Date().getMonth() + 1).toString()]).toFixed(2)),
                        total: formatCS(total)
                    });
                });
            });
        }).catch(function(e){
            console.log("error : " + e.message);
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//EDIT OUTPUT ROUTE
app.post('/reci/editOutput/',function(req, res){
    if(auth){
        admin.database().ref('recipes/' + req.body.guid).update({
            outputN : req.body.outputN,
            outputU : req.body.outputU
        },function(error){
            if(error){
                console.log("error : " + error);
            }else{
                res.end("success");
            }
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//ADD INGR RECI ROUTE
app.post('/reci/addingr/',function(req, res){
    if(auth){
        var dbRefObjectONEINGR = admin.database().ref('ingredients/' + req.body.guidingr);
        dbRefObjectONEINGR.once("value").then(function(snapi){
            admin.database().ref('recipesIngredients/' + req.body.guidrecipe + "/" + req.body.guidingr).set({
                guid: req.body.guidingr,
                name: snapi.val().name,
                price: snapi.val().price,
                quantity: snapi.val().quantity,
                unity: snapi.val().unity,
                usedN: 0,
                usedU: snapi.val().unity
            },function(error){
                calcRecipeCost(req.body.guidrecipe);
                if(error){
                    console.log("error : " + error);
                }else{
                    res.end("success");
                }
            });
        });
        admin.database().ref( "ingredientsLookup/" + req.body.guidingr + "/" + req.body.guidrecipe).set(
            true
        ,function(error){
            if(error){
                console.log("error : " + error);
            }else{
                res.end("success");
            }
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//DELETE INGR RECI ROUTE
app.post('/reci/delingr/',function(req, res){
    if(auth){
        admin.database().ref('recipesIngredients/' + req.body.guidrecipe +"/"+ req.body.guidingr).remove().then(function(){
            admin.database().ref('ingredientsLookup/'+ req.body.guidingr +"/"+ req.body.guidrecipe).remove().then(function(){
                calcRecipeCost(req.body.guidrecipe);
                res.end("success");
            }).catch(function(e){
                console.log("error : " + e.message);
            });
        }).catch(function(e){
            console.log("error : " + e.message);
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//EDIT INGR RECI ROUTE
app.post('/reci/editingr/',function(req, res){
    if(auth){
        admin.database().ref('recipesIngredients/' + req.body.guidrecipe + "/" + req.body.guidingr).update({
            usedN: req.body.usedN,
            usedU: req.body.usedU
        },function(error){
            calcRecipeCost(req.body.guidrecipe);
            if(error){
                console.log("error : " + error);
            }else{
                res.end("success");
            }
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//ADD RECI RECI ROUTE
app.post('/reci/addreci/',function(req, res){
    if(auth){
        var dbRefObjectONERECI = admin.database().ref('recipes/' + req.body.guidreci);
        dbRefObjectONERECI.once("value").then(function(snapr){
            admin.database().ref('recipesRecipes/' + req.body.guidrecipe + "/" + req.body.guidreci).set({
                guid: req.body.guidreci,
                name: snapr.val().name,
                cost: snapr.val().price,
                outputN: snapr.val().outputN,
                outputU: snapr.val().outputU,
                usedN : snapr.val().outputN,
                usedU : snapr.val().outputU,
                price : snapr.val().price
            },function(error){
                calcRecipeCost(req.body.guidrecipe);
                if(error){
                    console.log("error : " + error);
                }else{
                    res.end("success");
                }
            });
        });
        admin.database().ref("recipesLookup/" + req.body.guidreci + "/" + req.body.guidrecipe).set(
            true
        ,function(error){
            if(error){
                console.log("error : " + error);
            }else{
                res.end("success");
            }
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//DELETE RECI RECI ROUTE
app.post('/reci/delreci/',function(req, res){
    if(auth){
        admin.database().ref('recipesRecipes/' + req.body.guidrecipe +"/"+ req.body.guidreci).remove().then(function(){
            admin.database().ref('recipesLookup/'+ req.body.guidreci +"/"+ req.body.guidrecipe).remove().then(function(){
                calcRecipeCost(req.body.guidrecipe);
                res.end("success");
            }).catch(function(e){
                console.log("error : " + e.message);
            });
        }).catch(function(e){
            console.log("error : " + e.message);
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//EDIT INGR RECI ROUTE
app.post('/reci/editreci/',function(req, res){
    if(auth){
        admin.database().ref('recipesRecipes/' + req.body.guidrecipe + "/" + req.body.guidreci).update({
            usedN: req.body.usedN,
            usedU: req.body.usedU
        },function(error){
            calcRecipeCost(req.body.guidrecipe);
            if(error){
                console.log("error : " + error);
            }else{
                res.end("success");
            }
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//GET ALLEGRGENIC
app.post('/reci/allergenics/',function(req, res){
    if(auth){
        var dbRefObjectALLERGENICS = admin.database().ref('recipes/' + req.body.guid + "/allergenics");
        dbRefObjectALLERGENICS.once("value").then(function(snapr){
            res.end(snapr.val().filter(Boolean).join());
        }).catch(function(e){
            console.log("error : " + e.message);
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//EDIT ALLERGENIC RECI ROUTE
app.post('/reci/editallergenics/',function(req, res){
    if(auth){
        var allergenicItems = req.body.allergenicItemsString.split(",");
        var dbRefObjectONERECI = admin.database().ref('ingredients/' + req.body.guidingr);
        dbRefObjectONERECI.once("value").then(function(snapi){
            admin.database().ref('recipes/' + req.body.guidrecipe +"/allergenics").update({
                1 : allergenicItems[0],
                2 : allergenicItems[1],
                3 : allergenicItems[2],
                4 : allergenicItems[3],
                5 : allergenicItems[4],
                6 : allergenicItems[5],
                7 : allergenicItems[6],
                8 : allergenicItems[7],
                9 : allergenicItems[8],
                10 : allergenicItems[9],
                11 : allergenicItems[10],
                12 : allergenicItems[11],
                13 : allergenicItems[12],
                14 : allergenicItems[13]
            },function(error){
                if(error){
                    console.log("error : " + error);
                }else{
                    res.end("success");
                }
            });
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//#endregion RECIPES

//#region PRESTATIONS

/////////////////
// PRESTATIONS //
/////////////////

//MAIN PAGE PRESTATION
app.post('/pres/',function(req, res){
    console.log("pres");
    res.render('pres.ejs');
});

//#endregion PRESTATIONS

//#region CLIENTS

/////////////
// CLIENTS //
/////////////

//MAIN PAGE CLIENTS
app.post('/clie/',function(req, res){
    console.log("clie");
    res.render('clie.ejs');
});

//#endregion CLIENTS

//#region SETTINGS

//////////////
// SETTINGS //
//////////////

//MAIN PAGE SETTINGS
app.post('/sets/',function(req, res){
    const dbRefObjectSETS = admin.database().ref().child("settings");
    dbRefObjectSETS.on("value", function(snap){
        console.log("sets");
        res.render('sets.ejs',{
            data : snap.val()
        });
    });
});

app.post('/sets/edit',function(req, res){
    if(auth){
        admin.database().ref('settings/').update({
            month1 : req.body.month1,
            month2 : req.body.month2,
            month3 : req.body.month3,
            month4 : req.body.month4, 
            month5 : req.body.month5,
            month7 : req.body.month7,
            month8 : req.body.month8,
            month6 : req.body.month6,
            month9 : req.body.month9,
            month10 : req.body.month10,
            month11 : req.body.month11,
            month12 : req.body.month12,
            marge : req.body.marge,
            rent : req.body.rent,
            elec : req.body.elec,
            water : req.body.water
        },function(error){
            if(error){
                console.log("error : " + error);
            }else{
                res.end("success");
            }
        });
    }else{
        res.end("AUTHENTIFICATION NEEDED !");
    }
});

//#endregion SETTINGS

//#region OTHERS

///////////
// OTHER //
///////////

//CHECKAUTH
app.post('/checkauth',function(req, res){
    res.end(auth.toString());
});

//AUTH
app.post('/auth/',function(req, res){
    var hash = sha256(req.body.pass);
    if(hash.toLowerCase() == key.toLowerCase()){
        auth = true;
        console.log("auth TRUE");
    }else{
        auth = false;
        console.log("auth FALSE");
    }
    res.end(auth.toString());
});

//KILLAUTH
app.post('/killauth/',function(req, res){
    auth = false;
    res.end(auth.toString());
});

//PAGE INDEX
app.get('/',function(req, res){
    console.log("index");
    res.sendFile(path.join(__dirname+'./public/index.html'));
});

//404
app.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain');
    res.sendStatus(404);
    res.end('Page introuvable !');
});

exports.app = functions.https.onRequest(app);

//#endregion OTHERS

//#region FUNCTIONS

//CREER UN GUID
function createGuid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function convertIngrPrice(ingrQfrom, ingrUfrom, ingrP, ingrQto, ingrUto){
    var fac = 0;
    if(ingrUfrom=="g" && ingrUto=="mL"){fac = 1;}
    if(ingrUfrom=="mL" && ingrUto=="g"){fac = 1;}
    if(ingrUfrom=="Kg" && ingrUto=="L"){fac = 1;}
    if(ingrUfrom=="L" && ingrUto=="Kg"){fac = 1;}

    if(ingrUfrom=="mL" && ingrUto=="Kg"){fac = 1000;}
    if(ingrUfrom=="Kg" && ingrUto=="mL"){fac = 0.001;}
    if(ingrUfrom=="L" && ingrUto=="g"){fac = 0.001;}
    if(ingrUfrom=="g" && ingrUto=="L"){fac = 1000;}

	if(ingrUfrom=="g" && ingrUto=="g"){fac = 1;}
	if(ingrUfrom=="g" && ingrUto=="Kg"){fac = 1000;}
	if(ingrUfrom=="Kg" && ingrUto=="g"){fac = 0.001;}
	if(ingrUfrom=="Kg" && ingrUto=="Kg"){fac = 1;}
	if(ingrUfrom=="mL" && ingrUto=="L"){fac = 1000;}
	if(ingrUfrom=="mL" && ingrUto=="mL"){fac = 1;}
	if(ingrUfrom=="L" && ingrUto=="L"){fac = 1;}
	if(ingrUfrom=="L" && ingrUto=="mL"){fac = 0.001;}
	if(ingrUfrom=="cm" && ingrUto=="cm"){fac = 1;}
	if(ingrUfrom=="cm" && ingrUto=="m"){fac = 100;}
	if(ingrUfrom=="m" && ingrUto=="m"){fac = 1;}
	if(ingrUfrom=="m" && ingrUto=="cm"){fac = 0.01;}
	if(ingrUfrom=="unité" && ingrUto=="unité"){fac = 1;}
	if(fac==0){
        console.log("ERROR !!");
        console.log("conversion de " + ingrQfrom + " " + ingrUfrom + "(" + ingrP + ") pour " + ingrQfrom + " " + ingrUfrom + " => " + res);
        return "error, no match bewteen these ingr units";}
    var res = (ingrQto/(ingrQfrom/fac))*ingrP;
	return res;
}

function convertReciPrice(reciQfrom, reciUfrom, reciP, reciQto, reciUto){
    var fac = 0;
    if(reciUfrom=="g" && reciUto=="mL"){fac = 1;}
    if(reciUfrom=="mL" && reciUto=="g"){fac = 1;}
    if(reciUfrom=="Kg" && reciUto=="L"){fac = 1;}
    if(reciUfrom=="L" && reciUto=="Kg"){fac = 1;}

    if(reciUfrom=="mL" && reciUto=="Kg"){fac = 1000;}
    if(reciUfrom=="Kg" && reciUto=="mL"){fac = 0.001;}
    if(reciUfrom=="L" && reciUto=="g"){fac = 0.001;}
    if(reciUfrom=="g" && reciUto=="L"){fac = 1000;}

	if(reciUfrom=="g" && reciUto=="g"){fac = 1;}
	if(reciUfrom=="g" && reciUto=="Kg"){fac = 1000;}
	if(reciUfrom=="Kg" && reciUto=="g"){fac = 0.001;}
	if(reciUfrom=="Kg" && reciUto=="Kg"){fac = 1;}
	if(reciUfrom=="mL" && reciUto=="L"){fac = 1000;}
	if(reciUfrom=="mL" && reciUto=="mL"){fac = 1;}
	if(reciUfrom=="L" && reciUto=="L"){fac = 1;}
	if(reciUfrom=="L" && reciUto=="mL"){fac = 0.001;}
	if(reciUfrom=="parts" && reciUto=="parts"){fac = 1;}
    if(fac==0){
        console.log("ERROR !!");
        console.log("conversion de " + reciQfrom + " " + reciUfrom + "(" + reciP + ") pour " + reciQfrom + " " + reciUfrom + " => " + res);
        return "error, no match bewteen these reci units";}
    var res = (reciQto/(reciQfrom/fac))*reciP;
	return res;
}

function formatCS(n){
    return parseFloat(Math.round(n * 100) / 100).toFixed(2);
}

/*
    -trigger a l'edit d'un ingr sur : les recette utilisant l'ingr
    -trigger au delete d'un ingr sur : les recette utilisant l'ingr
    -trigger a l'ajout d'un ingr dans une recette sur : cette recette
    -trigger a l'edit d'un ingr dans une recette sur : cette recette
    -trigger au delete d'un ingr dans une recette sur : cette recette
    -trigger a l'ajout d'une recette dans une recette sur : cette recette
    -trigger a l'edit d'une recette dans une recette sur : cette recette
    -trigger au delete d'une recette dans une recette sur : cette recette
*/
function calcRecipeCost (guid){
    var total = 0;
    var dbRefObjectRECI = admin.database().ref('recipes/' + guid)
    var dbRefObjectRECIINGR = admin.database().ref('recipesIngredients/' + guid);
    var dbRefObjectRECIRECI = admin.database().ref('recipesRecipes/' + guid);
    var dbRefObjectRECLOOKUP = admin.database().ref('recipesLookup/' + guid);

    dbRefObjectRECIINGR.once("value").then(function(snapri){//Calcul cost total ingr
        for(var rIngr in snapri.val()){
            var ingr = snapri.val()[rIngr];
            total = parseFloat(total) + parseFloat(formatCS(convertIngrPrice(ingr.quantity,ingr.unity,ingr.price,ingr.usedN,ingr.usedU)));
        }
        dbRefObjectRECIRECI.once("value").then(function(snaprr){//Calcul cost total reci
            for(var rReci in snaprr.val()){
                var reci = snaprr.val()[rReci];
                total = parseFloat(total) +  parseFloat(formatCS(convertReciPrice(reci.outputN,reci.outputU,reci.cost,reci.usedN,reci.usedU)));    
            }
            dbRefObjectRECI.update({//update price recipe
                price : formatCS(total)
            }).then(function(){
                dbRefObjectRECLOOKUP.once("value", snaplookup => {
                    snaplookup.forEach(snaplkp => {//pour chaque reci integrant cette reci
                        admin.database().ref("recipesRecipes/" + snaplkp.key + "/" + guid).once("value", snaplkprr => {
                            var recr = snaplkprr.val();
                            console.log(JSON.stringify(recr));
                            console.log("outputN : " + recr.outputN);
                            console.log("convert lookup : " + recr.outputN + "/" + recr.outputU + "/" + total + "/" + recr.usedN + "/" + recr.usedU);
                            admin.database().ref("recipesRecipes/" + snaplkp.key + "/" + guid).update({
                                cost : formatCS(convertReciPrice(recr.outputN,recr.outputU,total,recr.usedN,recr.usedU)),
                                price : formatCS(total)
                            });
                        });
                    });
                }).then(function(){
                    console.log("repercution : ");
                    dbRefObjectRECLOOKUP.once("value", snaplookup => {
                        snaplookup.forEach(snaplkp => {
                            calcRecipeCost(snaplkp.key);
                        });
                    });
                });
            });
        });    
    });
}
//#endregion FUNCTIONS